#!/bin/bash

NODE_CONFIG_JSON="/root/nodeconfig.json"
DEFAULT_CAPABILITIES_JSON="/root/defaultcapabilities.json"
APPIUM_LOG="${APPIUM_LOG:-/var/log/appium.log}"
CMD="xvfb-run appium --log $APPIUM_LOG"

# Use existing adb keys if set in the environment
if [ ! -z "${ADBKEY}" ]; then
    # gitlab breaks line endings for multiline variables so fix with sed
    echo "${ADBKEY}" | sed 's/\r /\n/g' > ~/.android/adbkey
fi

if [ ! -z "${ADBKEY_PUB}" ]; then
    echo ${ADBKEY_PUB} > ~/.android/adbkey.pub
fi

if [ ! -z "${SALT_MASTER}" ]; then
    echo "[INIT] ENV SALT_MASTER it not empty, salt-minion will be prepared"
    echo "master: ${SALT_MASTER}" >> /etc/salt/minion
    salt-minion &
    echo "[INIT] salt-minion is running..."
fi

if [ "$ATD" = true ]; then
    echo "[INIT] Starting ATD..."
    java -jar /root/RemoteAppiumManager.jar -DPort=4567 &
    echo "[INIT] ATD is running..."
fi

if [ "$REMOTE_ADB" = true ]; then
    /root/wireless_connect.sh
fi

# support forwarding tcp with rinetd (typically for mitmproxy)
if [ ! -z "${PORTFORWARDS}" ]; then
    # potentially multiline, format for each line:
    # bindadress  bindport  connectaddress  connectport
    echo -e "${PORTFORWARDS}" >> /etc/rinetd.conf
    rinetd
    # Also forward the ports to all connected devices with adb
    for ADB_DEVICE in $(adb devices | grep "device$" | awk {'print $1'})
    do
        while IFS= read -r LINE; do
            LOCALPORT=$(echo $LINE | awk {'print $2'})
            adb -s ${ADB_DEVICE} reverse tcp:${LOCALPORT} tcp:${LOCALPORT}
        done <<< "$PORTFORWARDS"
        echo "Port forwards for device ${ADB_DEVICE}:"
        adb -s ${ADB_DEVICE} reverse --list
    done
fi

if [ "$CONNECT_TO_GRID" = true ]; then
    if [ "$CUSTOM_NODE_CONFIG" != true ]; then
        /root/generate_config.sh $NODE_CONFIG_JSON
    fi
    CMD+=" --nodeconfig $NODE_CONFIG_JSON"
fi

if [ "$DEFAULT_CAPABILITIES" = true ]; then
    CMD+=" --default-capabilities $DEFAULT_CAPABILITIES_JSON"
fi

if [ "$RELAXED_SECURITY" = true ]; then
    CMD+=" --relaxed-security"
fi

if [ "$CHROMEDRIVER_AUTODOWNLOAD" = true ]; then
    CMD+=" --allow-insecure chromedriver_autodownload"
fi

if [ "$ADB_SHELL" = true ]; then
    CMD+=" --allow-insecure adb_shell"
fi

pkill -x xvfb-run
rm -rf /tmp/.X99-lock

$CMD
