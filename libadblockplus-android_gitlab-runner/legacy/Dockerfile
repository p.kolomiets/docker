# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:18.04

RUN apt-get update -qyy && \
    apt-get install -qyy \
    sudo \
    dumb-init \
    curl wget \
    p7zip-full unzip \
    python \
    npm \
    openjdk-8-jdk \
    build-essential clang libc++-dev libc++abi-dev

WORKDIR /opt

RUN wget --quiet https://dl.google.com/android/repository/android-ndk-r16b-linux-x86_64.zip -O ./android-ndk.zip && \
    unzip -q ./android-ndk.zip -d ./ && \
    rm ./android-ndk.zip

RUN wget --quiet https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O ./sdk-tools.zip && \
    unzip -q ./sdk-tools.zip -d ./android-sdk && \
    rm ./sdk-tools.zip

RUN echo y | /opt/android-sdk/tools/bin/sdkmanager "build-tools;28.0.3" "platforms;android-28" | grep -v =

ENV ANDROID_HOME=/opt/android-sdk
ENV ANDROID_NDK_ROOT=/opt/android-ndk-r16b
ENV ANDROID_NDK_HOME=/opt/android-ndk-r16b

RUN curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash

COPY pin-gitlab-runner.pref /etc/apt/preferences.d/pin-gitlab-runner.pref

RUN apt-get install -qyy gitlab-runner

RUN adduser --gecos "" --disabled-password ci_user && \
    usermod -aG sudo ci_user && \
    echo "ci_user ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers && \
    mkdir /opt/ci && \
    chown -R ci_user:ci_user /opt/ci

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD ["gitlab-runner", "run", "--working-directory", "/opt/ci", "--user", "ci_user"]
